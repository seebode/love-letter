import java.util.ArrayList;
import java.util.Scanner;

/**
 * The class is used as a Library for all other classes
 */
public class Library {

    /**
     * Asks the calling player for a card in his/her hand. The function call
     * {@link #differentiateInputCommand(Scanner scanner, Player player, String message)} differentiates if the
     * input was a command or not. This gives the player the opportunity to call helper commands like s "\showHands" while selecting a card.
     * @param player The Player who is selecting the card
     * @param scanner the scanner which is handed over to {@link #differentiateInputCommand(Scanner scanner, Player player, String message)}
     * @param message the message printed from the calling function
     * @return the card the player selected
     */
    public static Card selectCard(Player player, Scanner scanner, String message){
        String cardInputName;
        Card card = null;
        boolean isFound = false;
        while(!isFound) {
            cardInputName = (differentiateInputCommand(scanner, player, message)).toLowerCase();

            for (Card cardInHand : player.hand) {
                if(cardInHand.name.toLowerCase().equals(cardInputName) || cardInHand.shortName.toLowerCase().equals(cardInputName)) {
                    isFound = true;
                    card = cardInHand;
                }
            }

            if(card == null)
                System.out.println("Error: Card not found on you hand! Maybe typo?\nTry again:");
        }

        return card;
    }

    /**
     * Asks the calling player for a player to target. The function call
     * {@link #differentiateInputCommand(Scanner scanner, Player player, String message)} differentiates if the
     * input was a command or not. This gives the player the opportunity to call helper commands like s "\showPlayers" while selecting a target.
     * @param isSelfTargetingOn  a flag showing if the calling function allows the player, who owns the card, to target himself
     * @param owningPlayer the player owning the card, which needs a target
     * @param currentPlayerOrder the current playing order
     * @param scanner the scanner which is handed over to {@link #differentiateInputCommand(Scanner scanner, Player player, String message)}
     * @return the targeted player
     */
    public static Player targetPlayer(boolean isSelfTargetingOn, Player owningPlayer, ArrayList<Player> currentPlayerOrder, Scanner scanner){
        System.out.println("Who is your target? Type in the player's name:");

        Player targetedPlayer = null;
        while(true){
            String inputName = differentiateInputCommand(scanner, owningPlayer, "Who is your target? Type in the player's name:");

            for (Player player : currentPlayerOrder) {
                if(player.name.equals(inputName)) {
                    targetedPlayer = player;
                    break;
                }
            }

            if(targetedPlayer == null)
                System.out.println("Error: The player specified doesn't exist or the name typed in has a typo! Try again! ");
            else if(targetedPlayer.isKnockedOutOfRound)
                System.out.println("Error: The player specified is knocked out of this round! Choose another one");
            else if (targetedPlayer.isProtectedByHandmaid) {
                System.out.println("Error: The player specified is protected by Handmaiden Susannah! Choose another one");
            } else if(!isSelfTargetingOn && targetedPlayer == owningPlayer)
                System.out.println("Error: You can't target yourself! Try someone else");
            else
                break;
        }
        return targetedPlayer;
    }

    public static void makeSpaceInConsole(){
        System.out.println("\n\n\n\n\n\n\n\n\n");
    }

    public static int getPlayerCount(Scanner scanner){
        System.out.println("How many players are there? 2 - 4 players are allowed!");
        int playerCount;

        while (true) {
            try {
                playerCount = Integer.parseInt(scanner.nextLine());
            }catch(Exception e){
                System.out.println("Error: Input is not a number! Please try again\n");
                continue;
            }

            if (playerCount < 2 || playerCount > 4)
                System.out.println("Error: Too many or not enough players!");
            else
                break;
        }
        return playerCount;
    }

    /**
     * Takes input from the console and differentiates if the input was a command or not. If it was a command,
     * the function will repeat to wait for input until the input was not a command
     * @param scanner for getting input of the console
     * @param player the player giving the input
     * @param message the message of the calling function to be printed out if input was a command
     * @return the input if it wasn't a command
     */
    private static String differentiateInputCommand(Scanner scanner, Player player, String message){
        String input;

        while(true){
            input = scanner.nextLine();

            if(input.contains("\\")) {
                CommandHandler.interpretCommand(input, scanner, player, true);
                System.out.println(message);
            }
            else
                return input;
        }
    }


}
