public class Card {
    /**
     * This interface purpose is to have the card-effects as objects, so every card has a card-effect as one of its properties.
     * When initializing a card, the card-effect can be handed over as a lambda function. I implemented this, so I don't have to use inheritance.
     */
    public interface CardEffect{
        void discardEffect(Player owningPlayer);
    }

    int number;
    String name;
    String shortName;
    String effectDescription;
    CardEffect cardEffect;

    public Card(int number, String name, String shortName, String effectDescription, CardEffect cE){
        this.number = number;
        this.name = name;
        this.shortName = shortName;
        this.effectDescription = effectDescription;
        this.cardEffect = cE;
    }

    public void triggerEffect(Player owningPlayer){
        this.cardEffect.discardEffect(owningPlayer);
    }

    /**
     * shows the card-description along with its number and occurrence in the deck
     */
    public void showDescription(){
        int occurrenceInDeck = switch (this.shortName) {
            case "princess" -> 1;
            case "king" -> 1;
            case "guard" -> 5;
            default -> 2;
        };

        System.out.println("Description of card " + this.name + ":" +
                "\n" + this.effectDescription +
                "\nCard number: " + this.number +
                "\nCard occurrence in Deck: " + occurrenceInDeck);
    }

}
