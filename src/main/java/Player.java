import java.util.ArrayList;
import java.util.Scanner;

public class Player {

    String name;
    boolean isKnockedOutOfRound = false;
    boolean isProtectedByHandmaid = false;
    ArrayList<Card> hand = new ArrayList<>();
    ArrayList<Card> discardPile = new ArrayList<>();
    int tokenOfLoveCount = 0;

    public Player(String name){
        this.name = name;
    }

    /**
     * Ask the Player object for a card of her/his hand and discards it and trigger its card effect
     * @param scanner To hand a scanner over to {@link Library#selectCard(Player player, Scanner scanner, String message)}
     */
    public void playCard(Scanner scanner){
        String message = "Which card do you want to discard? Type in the name of the card:" +
                "\nFor example type in \"princess\" for discarding Princess Annette or \"guard\" for guard Odette";
        System.out.println(message);

        Card card = Library.selectCard(this, scanner, message);

        hand.remove(card);
        card.triggerEffect(this);
        discardPile.add(card);
    }

    public void showHand(){
        System.out.println("Hand of player " + this.name + ":");

        for(Card card : this.hand){
            System.out.println("-  " + card.name);
        }
    }

    /**
     * gets the sum of the current hand and discardPile of the player
     * @return returns the sum of the current hand and discardPile of the player
     */
    public int getScoreWithDiscardPile(){
        int score = hand.get(0).number;

        for (Card card : discardPile) {
            score += card.number;
        }

        return score;
    }
}
