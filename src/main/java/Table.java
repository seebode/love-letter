import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Scanner;

/**
 * The Table safes and modifies the game-state and defines the game-logic.
 * The game-state is mainly made out of objects of two other classes: {@link Player} and {@link Card}
 */

public class Table {
    private static ArrayList<Card> deck = new ArrayList<>();
    public static ArrayList<Player> listOfPlayers = new ArrayList<>();
    public static ArrayList<Card> cardsPutAside = new ArrayList<>();
    public static boolean isOpenForPlayerCommands;

    /**
     * lets every player run their turn in the current playing order after checking if the round should end
     * @param currentPlayingOrder the current playing order
     * @param scanner the only scanner there is
     * @return the same playing order if there was no winner or the new playing order starting with the most recent winner
     */
    public static ArrayList<Player> runEveryPlayerOneTurn(ArrayList<Player> currentPlayingOrder, Scanner scanner){
        Library.makeSpaceInConsole();

        for (Player currentPlayer : currentPlayingOrder) {
            if(deck.isEmpty()){
                System.out.println("The deck is empty! End of this Round!");
                return reorderPlayingOrder(currentPlayingOrder, calculateRoundWinner(currentPlayingOrder, scanner));
            }
            else if (checkEveryOtherPlayerKnockedOut(currentPlayer)) {
                System.out.println("Every other player is knocked out of the game. You win this round!");
                currentPlayer.tokenOfLoveCount += 1;
                findGameWinner(scanner);
                prepareNewRound(scanner);
                return reorderPlayingOrder(currentPlayingOrder, currentPlayer);
            }

            if(currentPlayer.isKnockedOutOfRound)
                System.out.println("Sry, " + currentPlayer.name + " is knocked out of this round! Wait for the next one");
            else
                takeTurn(currentPlayer, scanner);
        }
        //if nobody wins in one round the first player should start again
        return currentPlayingOrder;
    }



    private static boolean checkEveryOtherPlayerKnockedOut(Player playerToBeExcluded){
        for (Player activePlayer : listOfPlayers) {
            if(activePlayer == playerToBeExcluded)
                continue;
            if(!activePlayer.isKnockedOutOfRound)
                return false;
        }
        return true;
    }

    /**
     * calculates the winner of a round
     * @param currentPlayingOrder the current playing order
     * @param scanner the only scanner there is
     * @return the winner of the round or, if there is a tie even after calculating the winner including the discard piles, the player who won the most recent round
     */
    private static Player calculateRoundWinner(ArrayList<Player> currentPlayingOrder, Scanner scanner){
        System.out.println("Calculating winner!");
        ArrayList<Player> onlyPlayersStillInRound = new ArrayList<>();

        for (Player player : currentPlayingOrder) {
            if(!player.isKnockedOutOfRound)
                onlyPlayersStillInRound.add(player);
        }

        Player winner = onlyPlayersStillInRound.get(0);
        ArrayList<Player> allWinners = new ArrayList<>();

        for (int i = 1; i < onlyPlayersStillInRound.size(); i++) {
            if(winner.hand.get(0).number < onlyPlayersStillInRound.get(i).hand.get(0).number)
                winner = onlyPlayersStillInRound.get(i);
        }

        allWinners.add(winner);

        for (Player player : onlyPlayersStillInRound) {
            if (winner.hand.get(0).number == player.hand.get(0).number && winner != player) {
                allWinners.add(player);
            }
        }

        Player winnerResult;
        //calculate with discard piles
        if(allWinners.size() != 1){
            System.out.println("WOW, there are multiple winners. The winner with the biggest sum \n" +
                    "of discarded cards and hand card wins. Otherwise all winners win!");

            Player newSingleWinner = allWinners.get(0);

            for (int i = 1; i < allWinners.size(); i++) {
                if(newSingleWinner.getScoreWithDiscardPile() < allWinners.get(i).getScoreWithDiscardPile())
                    newSingleWinner = onlyPlayersStillInRound.get(i);
            }

            allWinners.clear();
            allWinners.add(newSingleWinner);

            for (Player player : onlyPlayersStillInRound) {
                if (newSingleWinner.getScoreWithDiscardPile() == player.getScoreWithDiscardPile() && newSingleWinner != player) {
                    allWinners.add(player);
                }
            }
            //still tie -> most recent winner goes first
            if(allWinners.size() != 1){
                System.out.println("There is still a tie! Winners are: ");

                for (Player player : allWinners) {
                    System.out.println("- " + player.name);
                    player.tokenOfLoveCount += 1;
                }
                //TODO better logging
                System.out.println("All winners got new tokens!");

                System.out.println("The winner who went on the most recent date (" + currentPlayingOrder.get(0).name +") goes first!");
                return currentPlayingOrder.get(0);


            }
            else {
                System.out.println(allWinners.get(0).name + " won! Token of love: " + allWinners.get(0).tokenOfLoveCount + " + 1");
                allWinners.get(0).tokenOfLoveCount += 1;
                winnerResult = allWinners.get(0);
            }
        }
        else{
            System.out.println(allWinners.get(0).name + " won with: " + allWinners.get(0).hand.get(0).name +  " -> Token of love: " + allWinners.get(0).tokenOfLoveCount + " + 1");
            allWinners.get(0).tokenOfLoveCount += 1;
            winnerResult = allWinners.get(0);
        }
        findGameWinner(scanner);
        prepareNewRound(scanner);
        return winnerResult;
    }

    private static ArrayList<Player> reorderPlayingOrder(ArrayList<Player> currentPlayingOrder, Player winner){
        int indexOfWinner = currentPlayingOrder.indexOf(winner);

        List<Player> firstHalfOfNewOrderList = currentPlayingOrder.subList(indexOfWinner, currentPlayingOrder.size());
        List<Player> secondHalfOfNewOrderList = currentPlayingOrder.subList(0, indexOfWinner);

        ArrayList<Player> newPlayingOrder = new ArrayList<>(firstHalfOfNewOrderList);
        newPlayingOrder.addAll(secondHalfOfNewOrderList);

        return newPlayingOrder;
    }
    private static void takeTurn(Player player, Scanner scanner){
        Library.makeSpaceInConsole();
        System.out.println(player.name + "'s turn! You will draw a card from the deck." +
                "\nType in \"\\help\" to see all available commands. Some are very helpful!" +
                "\nOnce you are ready, type in \"\\playCard\" to play a card.");

        player.hand.add(deck.remove(0));

        String command;

        boolean isFinished = false;
        while(!isFinished) {
            command = scanner.nextLine();
            isFinished = CommandHandler.interpretCommand(command, scanner, player, false);
        }
    }

    /**
     * prepares a new round for the game based on how many players there are -> clears the current deck and cardsPutAside and every player discardPiles and hands
     * @param scanner a scanner to hand over to {@link #generateDeck(Scanner scanner)}
     */
    public static void prepareNewRound(Scanner scanner){
        int playerCount = listOfPlayers.size();

        deck.clear();
        cardsPutAside.clear();
        for (Player player : listOfPlayers) {
            player.discardPile.clear();
        }

        System.out.println("Generating cards\n\n");
        deck = generateDeck(scanner);

        System.out.println("Shuffling cards\n\n");
        Collections.shuffle(deck);

        if(playerCount == 2) {
            System.out.println("Since there are only 2 players, 1 card will be removed from the deck\n\n");
            cardsPutAside.add(deck.remove(0));
            System.out.println("Card removed: " + cardsPutAside.get(0).name);
        }
        else {
            System.out.println("""
                    Since the are more than 2 players, 4 cards will be removed from the deck

                    Cards put aside:""");
            for (int i = 0; i < 4; i++) {
                cardsPutAside.add(deck.remove(0));
                System.out.println("-  " + cardsPutAside.get(i).name);
            }
        }

        System.out.println("Every player will start with no cards. Then every player will draw 1 new card!");
        for (Player player : listOfPlayers) {
            player.hand.clear();
            player.hand.add(deck.remove(0));
            System.out.println("Drawing card for player " + player.name);
            player.isKnockedOutOfRound = false;
            player.isProtectedByHandmaid = false;
        }
    }
    private static ArrayList<Card> generateDeck(Scanner scanner) {
        ArrayList<Card> result = new ArrayList<>();

        Card princess = new Card(
                8,
                "Princess Annette",
                "princess",
                """
                        If you discard the Princess—no matter how or why—she has tossed your letter into the fire. You are immediately
                        knocked out of the round. If the Princess was discarded by a card effect, any remaining effects of that card do not
                        apply (you do not draw a card from the Prince, for example). Effects tied to being knocked out the round still apply
                        (eg. Constable, Jester), however.""",
                (Player owningPlayer) -> {
                    owningPlayer.isKnockedOutOfRound = true;
                    System.out.println("Princess Annette was discarded by " + owningPlayer.name + ". You are knocked out of this round!");
                }
        );
        result.add(princess);

        Card countess = new Card(
                7,
                "Countess Wilhelmina",
                "countess",
                """
                        Unlike other cards, which take effect when discarded, the text on the Countess applies while she is in your hand. In
                        fact, the only time it doesn’t apply is when you discard her.
                        If you ever have the Countess and either the King or Prince in your hand, you must discard the Countess. You do
                        not have to reveal the other card in your hand. Of course, you can also discard the Countess even if you do not have a
                        royal family member in your hand. The Countess likes to play mind games....""",
                (Player owningPlayer) -> System.out.println("""
                        Countess Wilhelmina was discarded.\s

                        The countess Wilhelmina has no discard effect!""")
        );
        result.add(countess);

        Card king = new Card(
                6,
                "King Arnaud IV",
                "king",
                "When you discard King Arnaud IV, trade the card in your hand with the card held by another player of your choice.\n" +
                        "You cannot trade with a player who is out of the round.",
                (Player owningPlayer) -> {
                    System.out.println("King Arnaud IV was discarded. \n");
                    if(lookForTargetablePlayers(owningPlayer)){
                        System.out.println("Oh no!  No targets left in this round! \nYour card will be discarded with no effect!");
                        return;
                    }

                    Player targetedPlayer = Library.targetPlayer(false, owningPlayer, listOfPlayers, scanner);

                    if(targetedPlayer != owningPlayer){
                        System.out.println(targetedPlayer.name + " and " + owningPlayer.name + " will switch cards");
                        Card cardToSwitchTarget = targetedPlayer.hand.remove(0);
                        Card cardToSwitchOwning = owningPlayer.hand.remove(0);

                        targetedPlayer.hand.add(cardToSwitchOwning);
                        owningPlayer.hand.add(cardToSwitchTarget);
                    }
                    else
                        System.out.println("Oh no! No other player left!\nYour card will be discarded with no effect!");
                }
        );
        result.add(king);

        Card prince = new Card(
                5,
                "Prince Arnaud",
                "prince",
                """
                        When you discard Prince Arnaud, choose one player still in the round (including yourself). That player discards his or
                        her hand (but doesn’t apply its effect, unless it is the Princess, see page 8) and draws a new one. If the deck is empty
                        and the player cannot draw a card, that player draws the card that was removed at the start of the round. If all other
                        players are protected by the Handmaid, you must choose yourself.""",
                (Player owningPlayer) -> {
                    System.out.println("Prince Arnaud was discarded.\n");
                    Player targetedPlayer;
                    if(lookForTargetablePlayers(owningPlayer)){
                        System.out.println("Oh no! No targets left in this round! The Prince will target yourself now!");
                        targetedPlayer = owningPlayer;
                    }
                    else
                        targetedPlayer = Library.targetPlayer(true, owningPlayer, listOfPlayers, scanner);

                    Card cardToBeDiscarded = targetedPlayer.hand.remove(0);
                    targetedPlayer.discardPile.add(cardToBeDiscarded);
                    System.out.println(targetedPlayer.name + " will discard his/her card with no effect: "+ cardToBeDiscarded.name);

                    if (cardToBeDiscarded.equals(princess)) {
                        System.out.println("Since it is the princess, the effect will still take place!");
                        princess.triggerEffect(targetedPlayer);
                    }
                    else if(!deck.isEmpty()){
                        System.out.println("Drawing card from deck!");
                        targetedPlayer.hand.add(deck.remove(0));
                    }
                    else {
                        System.out.println("Drawing card from the cards put aside pile!");
                        targetedPlayer.hand.add(cardsPutAside.remove(0));
                    }
                }
        );
        result.add(prince);
        result.add(prince);

        Card handmaid = new Card(
                4,
                "Handmaid Susannah",
                "handmaid",
                "When you discard the Handmaid, you are immune to the effects of other players’ cards until the start of your next\n" +
                        "turn. If all players other than the player whose turn it is are protected by the Handmaid, the player must choose him or herself for a card’s effects, if possible.",
                (Player owningPlayer) -> {
                    owningPlayer.isProtectedByHandmaid = true;
                    System.out.println("Handmaid Susannah was discarded: " + owningPlayer.name + " is protected by Handmaid Susannah." +
                            " Nobody can target " + owningPlayer.name + " until it is " + owningPlayer.name + " turn again!");
                }
        );
        result.add(handmaid);
        result.add(handmaid);

        Card baron = new Card(
                3,
                "Baron Talus",
                "baron",
                """
                        When you discard the Baron, choose another player still in the round. You and that player secretly
                        compare your hands. The player with the lower number is knocked out of the round. In case of a
                        tie, nothing happens.""",
                (Player owningPlayer) -> {
                    System.out.println("Baron Talus was discarded.\n");
                    if(lookForTargetablePlayers(owningPlayer)){
                        System.out.println("Oh no! No targets left in this round! \nYour card will be discarded with no effect!");
                        return;
                    }
                    Player targetedPlayer = Library.targetPlayer(false, owningPlayer, listOfPlayers, scanner);


                    System.out.println(owningPlayer.name + " and " + targetedPlayer.name + " have to show each other their hands in secret!\n" +
                            "EVERYONE ELSE CLOSE YOUR EYES!");

                    try {
                        Thread.sleep(2000); //Pause the game for a little so everyone can close their eyes in time
                    } catch (InterruptedException e) {
                        System.out.println("Game Error:" + e.getMessage());
                    }

                    System.out.println("The player with the lower number is knocked out of the round. In case of a tie, nothing happens.");
                    owningPlayer.showHand();
                    targetedPlayer.showHand();

                    if(owningPlayer.hand.get(0).number > targetedPlayer.hand.get(0).number){
                        System.out.println(owningPlayer.name + " won! " + targetedPlayer.name + " will be knocked out of this round!");
                        targetedPlayer.isKnockedOutOfRound = true;
                    }
                    else if (targetedPlayer.hand.get(0).number > owningPlayer.hand.get(0).number){
                        System.out.println(targetedPlayer.name + " won!" + owningPlayer.name + " will be knocked out of this round!");
                        owningPlayer.isKnockedOutOfRound = true;
                    }
                    else{
                        System.out.println("TIE! Nothing happens!");
                    }
                }
        );
        result.add(baron);
        result.add(baron);

        Card priest = new Card(
                2,
                "Priest Tomas",
                "priest",
                "When you discard the Priest, you can look at another player’s hand. Do not reveal the\n" +
                        "hand to any other players.",
                (Player owningPlayer) -> {
                    System.out.println("Priest Tomas was discarded. \n");
                    if(lookForTargetablePlayers(owningPlayer)){
                        System.out.println("Oh no! No targets left in this round! \nYour card will be discarded with no effect!");
                        return;
                    }

                    Player targetedPlayer = Library.targetPlayer(false, owningPlayer, listOfPlayers, scanner);
                    System.out.println(targetedPlayer.name + " has to show " + owningPlayer.name + " his hand.\n " +
                            "THIS HAS TO BE IN SECRET! Close your eyes!");

                    try {
                        Thread.sleep(2000); //Pause the game for a little so everyone can close their eyes in time
                    } catch (InterruptedException e) {
                        System.out.println("Game Error:" + e.getMessage());
                    }

                    targetedPlayer.showHand();
                }
        );
        result.add(priest);
        result.add(priest);

        Card guard = new Card(
                1,
                "Guard Odette",
                "guard",
                """
                        When you discard the Guard, choose a player and name a number (other than 1). If
                        that player has that number in their hand, that player is knocked out of the round.
                        If all other players still in the round cannot be chosen (eg. due to Handmaid or
                        Sycophant), this card is discarded without effect.""",
                (Player owningPlayer) -> {
                    System.out.println("Guard Odette was discarded.");
                    if(lookForTargetablePlayers(owningPlayer)){
                        System.out.println("Oh no! Every other player is protected by the handmaid! \nYour card will be discarded with no effect!");
                        return;
                    }

                    Player targetedPlayer = Library.targetPlayer(false, owningPlayer, listOfPlayers, scanner);

                    System.out.println(owningPlayer.name + " choose a number. It has to be something else than 1!"
                            + " If " + targetedPlayer.name + " has the according card he/she will be knocked out of this round!");

                    int inputNumber;
                    while (true) {
                        try {
                            inputNumber = Integer.parseInt(scanner.nextLine());
                        } catch (Exception e) {
                            System.out.println("Error: Input is not a number! Please try again\n");
                            continue;
                        }

                        if(inputNumber <= 1) {
                            System.out.println("Error: Input is not is 1 or smaller than 1! Try something bigger!");
                            continue;
                        }
                        break;
                    }
                    Card targetedCard = targetedPlayer.hand.get(0);
                    if(targetedCard.number == inputNumber) {
                        System.out.println(targetedPlayer.name + " has the according card: " + targetedCard.name + "\nHe/She will now be knocked out of this round!");
                        targetedPlayer.isKnockedOutOfRound = true;
                    }
                    else
                        System.out.println(targetedPlayer.name + " has NOT the according card! NOTHING HAPPENS!");

                }
        );
        result.add(guard);
        result.add(guard);
        result.add(guard);
        result.add(guard);
        result.add(guard);

        return result;
    }
    private static boolean lookForTargetablePlayers(Player playerToBeExcluded){
        for (Player activePlayer : listOfPlayers) {
            if(activePlayer == playerToBeExcluded)
                continue;
            if(!activePlayer.isKnockedOutOfRound) {
                if (!activePlayer.isProtectedByHandmaid)
                    return false;
            }
        }
        return true;
    }
    private static void findGameWinner(Scanner scanner){
        int tokens;
        if(listOfPlayers.size() == 2)
            tokens = 7;
        else if (listOfPlayers.size() == 3)
            tokens = 5;
        else
            tokens = 4;

        for (Player activePlayer : listOfPlayers) {
            if (activePlayer.tokenOfLoveCount == tokens){
                System.out.println("WOW! " + activePlayer.name + " has " + tokens + " tokens of love!" +
                        "\n" + activePlayer.name + " has won the game!\n\n\n" +
                        "If you want to play again type on \"\\start\". To close the game \"\\quitGame\"");

                deck.clear();

                isOpenForPlayerCommands = false;
                boolean quitGame = false;
                while (!quitGame) {
                    String command = scanner.nextLine();
                    quitGame = CommandHandler.interpretCommand(command, scanner, listOfPlayers.get(0), false);
                }
            }
        }
    }

}