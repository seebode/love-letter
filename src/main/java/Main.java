import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("""
                Welcome to Love Letter!

                """);

        int playerCount = Library.getPlayerCount(scanner);

        System.out.println("\nCreating " + playerCount + " players:\n");

        for (int i = 1; i <= playerCount; i++) {
            System.out.println("Player " + i + " who are you?");
            String name = getName(scanner);

            Player p = new Player(name);

            Table.listOfPlayers.add(p);
        }

        System.out.println("The game is ready to start. Before starting you should checkout the available commands. " +
                "\nDo this by typing in \"\\help\". If you are ready to start the game type in \"\\start\"");


        while (true) {
            String command = scanner.nextLine();
            CommandHandler.interpretCommand(command, scanner, Table.listOfPlayers.get(0), false);
        }
    }

    private static String getName(Scanner scanner){
        String name;

        whileLoop: while(true){
            name = scanner.nextLine();
            if (name.contains("\\") || name.isEmpty()){
                System.out.println("Error: Name contains \"\\\" or is empty! Try again!");
                continue;
            }

            for (Player player : Table.listOfPlayers) {
                if(player.name.equals(name)){
                    System.out.println("Error: Another player already has this name! Please choose another one.");
                    continue whileLoop;
                }
            }

            return name;
        }
    }
}
