import java.util.ArrayList;
import java.util.Scanner;

/**
 * The CommandHandler handles all commands
 */
public class CommandHandler {

    /**
     * Interprets the given command and executes it if it is one, otherwise the function will call itself again asking for a new command.
     * This function is intended to be used inside a while-loop
     * @param command the command to be interpreted
     * @param scanner scanner to be handed over
     * @param player the player calling the command
     * @param disableStartPlay flag to signal if the command {@link #start(Scanner scanner)} and playCard should be disabled
     * @return returns true if the program should continue and false if the program allows new commands to be given
     */
    public static boolean interpretCommand(String command, Scanner scanner, Player player, boolean disableStartPlay){
        switch(command){
            case "\\help":
                Library.makeSpaceInConsole();
                System.out.println();
                System.out.println("""
                        available commands:\s
                        -  \\start                 will start the a new game
                        -  \\playCard              will play the current players card of choosing
                        -  \\showDiscardPiles      will show the discarded card piles of every player
                                                  including the cards removed from the deck at the beginning of each round
                        -  \\showCardDescription   will show the card description of the specified card including number and occurrence in the deck
                        -  \\showHand              will show the current players hand
                        -  \\showScore             will show the current Score of every player
                        -  \\showPlayers           will show the all the players names and if they are still in the current round
                        -  \\quitGame              will stop the game and turn off the program""");
                return false;

            case "\\start":
                Library.makeSpaceInConsole();
                if(!Table.isOpenForPlayerCommands && !disableStartPlay){
                    start(scanner);
                }
                else{
                    System.out.println("Game has already started! If you want to restart the game type in \"\\quitGame\" and start the programm again");
                }
                return false;

            case "\\playCard":
                Library.makeSpaceInConsole();
                if(Table.isOpenForPlayerCommands && !disableStartPlay) {
                    player.playCard(scanner);
                    return true;
                    }
                else {
                    System.out.println("You can not use command \"\\playCard\" right now.");
                    return false;
                }

            case "\\showHand":
                Library.makeSpaceInConsole();
                if(Table.isOpenForPlayerCommands)
                    player.showHand();
                else
                    System.out.println("You can not use command \"\\showHand\" right now.");
                return false;

            case "\\showScore":
                Library.makeSpaceInConsole();
                if(Table.isOpenForPlayerCommands)
                    showScore();
                else
                    System.out.println("You can not use command \"\\showScore\" right now.");
                return false;

            case "\\showDiscardPiles":
                Library.makeSpaceInConsole();
                if(Table.isOpenForPlayerCommands)
                    showDiscardPiles();
                else
                    System.out.println("You can not use command \"\\showDiscardPiles\" right now.");
                return false;

            case "\\showCardDescription":
                Library.makeSpaceInConsole();
                if(Table.isOpenForPlayerCommands)
                    showCardDescription(player, scanner);
                else
                    System.out.println("You can not use command \"\\showCardDescription\" right now.");
                return false;

            case "\\showPlayers":
                Library.makeSpaceInConsole();
                if(Table.isOpenForPlayerCommands)
                    showPlayers();
                else
                    System.out.println("You can not use command \"\\showPlayers\" right now.");
                return false;

            case "\\quitGame":
                System.out.println("Are you sure you want to quit the game? Type in \"yes\" for quitting the game or \"no\" to resume.");
                String answer;

                while (true){
                    answer = scanner.nextLine();
                     if (answer.equals("no")) {
                         System.out.println("Resuming game! Please enter new command:");
                         return false;
                     }
                     else if (answer.equals("yes")) {
                         System.out.println("Quitting game!");
                         scanner.close();
                         System.exit(0);
                     }

                     System.out.println("Error: Input was not \"yes\" or \"no\"! Please try again.");
                }

            default:
                System.out.println("Error: Input is not a command!");
                System.out.println("Please give new command:");

                command = scanner.nextLine();
                return interpretCommand(command, scanner, player, disableStartPlay);
        }

    }

    private static void start(Scanner scanner){
        Table.isOpenForPlayerCommands = true;
        ArrayList<Player> currentPlayingOrder = Table.listOfPlayers;

        for (Player player : Table.listOfPlayers) {
            player.tokenOfLoveCount = 0;
        }

        Table.prepareNewRound(scanner);

        while(true) {
            currentPlayingOrder = Table.runEveryPlayerOneTurn(currentPlayingOrder, scanner);
        }
    }
    private static void showScore(){
        System.out.println("The current score is:\n\n");

        for (Player player : Table.listOfPlayers) {
            System.out.println("-  " + player.name + ": Tokens of love " + player.tokenOfLoveCount);
        }
    }

    private static void showDiscardPiles(){
        System.out.println("Cards put aside from deck: ");
        for (Card card : Table.cardsPutAside) {
            if(card != null)
                System.out.println("-  " + card.name);
            else
                break;
        }

        System.out.println("\n\n\nDiscard piles of players:");
        for (Player player : Table.listOfPlayers) {
            System.out.println(player.name + ":");
            for (Card card : player.discardPile) {
                System.out.println("-  " + card.name);
            }
            System.out.println("\n");
        }
    }

    private static void showCardDescription(Player player, Scanner scanner){
        System.out.println("Which card do you want to see the description of?");

        Card card = Library.selectCard(player, scanner, "Which card do you want to see the description of?"); //TODO select card from all cards not just hand

        card.showDescription();
    }

    private static void showPlayers(){
        System.out.println("Players of this game are: ");

        for (Player player : Table.listOfPlayers) {
            String knockedOut;
            if(player.isKnockedOutOfRound)
                knockedOut = "knocked out of";
            else
                knockedOut = "still in";
            System.out.println("-  " + player.name + " is currently " + knockedOut + " this round.");
        }
    }
}
